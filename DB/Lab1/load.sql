USE [dbGameServer]
GO

BULK INSERT [dbGameServer].[dbo].[C]
FROM "D:\iu7-5sem-labs\DB\Lab1\characters.txt"
WITH (CODEPAGE = '646', DATAFILETYPE = 'char', FIELDTERMINATOR = '|', ROWTERMINATOR = '|')
GO

BULK INSERT [dbGameServer].[dbo].[A]
FROM "D:\iu7-5sem-labs\DB\Lab1\accounts.txt"
WITH (CODEPAGE = '646', DATAFILETYPE = 'char', FIELDTERMINATOR = '|', ROWTERMINATOR = '|')
GO

BULK INSERT [dbGameServer].[dbo].[S]
FROM "D:\iu7-5sem-labs\DB\Lab1\servers.txt"
WITH (CODEPAGE = '646', DATAFILETYPE = 'char', FIELDTERMINATOR = '|', ROWTERMINATOR = '|')
GO

BULK INSERT [dbGameServer].[dbo].[CAS]
FROM "D:\iu7-5sem-labs\DB\Lab1\links.txt"
WITH (CODEPAGE = '646', DATAFILETYPE = 'char', FIELDTERMINATOR = '|', ROWTERMINATOR = '|')
GO

USE	[master]
GO