﻿using System;
using System.Threading;

namespace Generators
{
    internal class RandomSingletone
    {
        public static int Next()
        {
            var res = random.Next();
            return res;
        }

        public static int Next(int maxValue)
        {
            var res = random.Next();
            return res;
        }

        public static int Next(int minValue, int maxValue)
        {
            var res = random.Next(minValue, maxValue);
            return res;
        }

        private static Random random = new Random();
    }
}
