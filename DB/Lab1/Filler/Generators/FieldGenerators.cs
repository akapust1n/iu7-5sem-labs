﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Generators
{
    internal class FieldGenerator
    {
        public static string GetInt (int min, int max)
        {
            return RandomSingletone.Next(min, max).ToString() + '|';
        }

        public static string GetHash()
        {
            return RandomSingletone.Next((int) 1e+6, (int) 2e+9).ToString() + '|';
        }

        public static string GetChar (int length)
        {
            var builder =  new StringBuilder();
            builder.Capacity = length + 1;
            builder.Append(Char.ToUpper(GetLetter()));
            for (int i = 1; i < length; ++i)
                builder.Append(GetLetter());

            return builder.ToString() + '|';
        }

        public static string GetDateTime()
        {
            return RandomSingletone.Next(2000, 2013).ToString() + '/' + RandomSingletone.Next(1, 12).ToString() + '/' + RandomSingletone.Next(0, 28) + '|';
        }

        private static char GetLetter()
        {
            int value = RandomSingletone.Next(0, 26);
            return (char) ('a' + value);
        }
    }

    internal class IncrementalId
    {
        public string GetId()
        {
            return id++.ToString() + '|';
        }

        public void Clear()
        {
            id = 1;
        }

        private int id = 1;
    }
}
