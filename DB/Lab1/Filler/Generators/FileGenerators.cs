﻿using System.IO;
using System.Text;

namespace Generators
{
    public abstract class FileGenerator
    {
        public FileGenerator (string filename)
        {
            if (File.Exists(filename))
                File.Delete(filename);
            fileStream = File.Create(filename);
        }

        public abstract void FillFile(int records);
        
        protected void Write(string text)
        {
            byte[] info = new ASCIIEncoding().GetBytes(text);
            fileStream.Write(info, 0, info.Length);
        }

        protected void WriteLine(string text)
        {
            Write(text);
            Write("\n");
        }

        private FileStream fileStream { get; set; }
    }

    public class CharactersGenerator: FileGenerator
    {
        public CharactersGenerator(string filename) : base(filename) { }

        public override void FillFile(int records)
        {
            for (int i = 0; i < records - 1; ++i)
                WriteLine(id.GetId() + FieldGenerator.GetChar(RandomSingletone.Next(5,10)) + FieldGenerator.GetInt(1, 101) + FieldGenerator.GetInt(1, 100000));
            Write(id.GetId() + FieldGenerator.GetChar(RandomSingletone.Next(5, 10)) + FieldGenerator.GetInt(1, 101) + FieldGenerator.GetInt(1, 100000));
        }

        private IncrementalId id = new IncrementalId();
    }

    public class AccountsGenerator: FileGenerator
    {
        public AccountsGenerator(string filename): base (filename) { }

        public override void FillFile(int records)
        {
            for (int i = 0; i < records - 1; ++i)
                WriteLine(id.GetId() + FieldGenerator.GetChar(RandomSingletone.Next(10, 15)) + FieldGenerator.GetHash() + FieldGenerator.GetDateTime());
            Write(id.GetId() + FieldGenerator.GetChar(RandomSingletone.Next(10, 15)) + FieldGenerator.GetHash() + FieldGenerator.GetDateTime());
        }

        private IncrementalId id = new IncrementalId();
    }

    public class ServersGenerator: FileGenerator
    {
        public ServersGenerator(string filename) : base(filename) { }

        public override void FillFile(int records)
        {
            for (int i = 0; i < records - 1; ++i)
                WriteLine(id.GetId() + FieldGenerator.GetChar(RandomSingletone.Next(5,10)) + FieldGenerator.GetInt(1000, 30000));
            Write(id.GetId() + FieldGenerator.GetChar(RandomSingletone.Next(5, 10)) + FieldGenerator.GetInt(1000, 30000));
        }

        private IncrementalId id = new IncrementalId();
    }

    public class LinkGenerator: FileGenerator
    {
        public LinkGenerator(string filename, int characters, int accounts, int servers): base (filename)
        {
            this.characters = characters;
            this.accounts = accounts;
            this.servers = servers;
        }

        public override void FillFile(int records)
        {
            int min = (records < characters) ? records : characters;

            for (int i = 1; i < min + 1; ++i)
                WriteLine(i.ToString() + '|' + FieldGenerator.GetInt(0, accounts) + FieldGenerator.GetInt(0, servers));
            Write((min - 1).ToString() + '|' + FieldGenerator.GetInt(0, accounts) + FieldGenerator.GetInt(0, servers));
        }

        private int characters;
        private int accounts;
        private int servers;
    }
}
