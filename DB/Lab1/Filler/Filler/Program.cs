﻿using System;
using Generators;

namespace Filler
{
    class Program
    {
        static void Main(string[] args)
        {
            new CharactersGenerator(@"characters.txt").FillFile(characters);
            new AccountsGenerator(@"accounts.txt").FillFile(accounts);
            new ServersGenerator(@"servers.txt").FillFile(servers);
            new LinkGenerator(@"links.txt", characters, accounts, servers).FillFile(characters);

            Console.WriteLine("Files was generated!");
            Console.WriteLine("Press 'Enter' to exit...");
            Console.ReadLine();
        }

        private static int characters = 1300;
        private static int accounts = 1000;
        private static int servers = 10;
    }
}
