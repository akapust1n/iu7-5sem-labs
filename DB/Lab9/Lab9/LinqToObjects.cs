﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9
{  
    class CData
    {
        public int Cno;
        public string Cname;
        public int Clvl;
        public int Cmoney;

        public void Print()
        {
            Console.WriteLine(String.Format("Cno: {0}, Cname: {1}, Clvl: {2}, Cmoney: {3}", Cno, Cname, Clvl, Cmoney));
        }
    }

    static class LinqToObjects
    {
        public static List<CData> Generate(int count)
        {
            int id = 1;

            var result = new List<CData>(count);

            for (; id <= count; ++id)
                result.Add
                    (new CData
                    {
                        Cno = id,
                        Cname = GetString(gener.Next(5, 10)),
                        Clvl = gener.Next(100),
                        Cmoney = gener.Next(200000)
                    });
            return result;
        }

        public static IEnumerable<string> Query1(List<CData> data)
        {
            return from record in data
                   where (record.Clvl > 50) && (record.Cmoney < 100000)
                   orderby record.Cno descending
                   select record.Cname;
        }

        public static int Query2(List<CData> data)
        {
            return (from val in data
                    let x = val.Cname
                    where x.Length > 6
                    select x).Count();
        }

        public static IEnumerable<int> Query3(List<CData> data1, List<CData> data2)
        {
            return from val1 in data1
                   join val2 in data2
                   on val1.Clvl equals val2.Clvl
                   where val1.Cmoney > 1000
                   select val1.Cmoney;
        }

        public static IEnumerable<IGrouping<int, CData>> Query4(List<CData> data)
        {
            return  from val in data
                    group val by val.Clvl into sameLevel
                    orderby sameLevel.Key ascending
                    select sameLevel;
        }

        public static void Query5(List<CData> data)
        {
            foreach (var d in data)
                d.Print();
        }

        private static string GetString(int length)
        {
            var builder =  new StringBuilder();
            builder.Capacity = length + 1;
            builder.Append(Char.ToUpper(GetLetter()));
            for (int i = 1; i < length; ++i)
                builder.Append(GetLetter());

            return builder.ToString();
        }

        private static char GetLetter()
        {
            int value = gener.Next(0, 26);
            return (char)('a' + value);
        }

        private static Random gener = new Random();
    }
}
