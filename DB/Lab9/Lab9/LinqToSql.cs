﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Lab9
{
    [Table(Name = "C")]
    internal class Character
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Cno { get; set; }

        [Column]
        public string Cname { get; set; }

        [Column]
        public short Clvl { get; set; }

        [Column]
        public int Cmoney { get; set; }

        public void Print()
        {
            Console.WriteLine(String.Format("Cno: {0}, Cname: {1}, Clvl: {2}, Cmoney: {3}", Cno, Cname, Clvl, Cmoney));
        }
    }

    [Table(Name = "A")]
    internal class Account
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Ano { get; set; }

        [Column]
        public string Aname { get; set; }

        [Column]
        public int Aphash { get; set; }

        [Column]
        public DateTime AcrDate { get; set; }
    }

    [Table(Name = "S")]
    internal class Server
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Sno { get; set; }

        [Column]
        public string Sname { get; set; }

        [Column]
        public int Smaxplayers { get; set; }
    }

    [Table(Name = "CAS")]
    internal class Link
    {
        [Column]
        public int Cno { get; set; }

        [Column]
        public int Ano { get; set; }

        [Column]
        public int Sno { get; set; }
    }

    class LinqToSql
    {

        public IEnumerable<string> Query1()
        {
            var serv = db.GetTable<Server>();

            return from s in serv
                   where s.Smaxplayers > 10000
                   select s.Sname;
        }

        public int Query2()
        {
            var chars = db.GetTable<Character>();
            var links = db.GetTable<Link>();
            var accounts = db.GetTable<Account>();

            return (from c in chars
                    join ln in links
                    on c.Cno equals ln.Cno
                    join a in accounts
                    on ln.Ano equals a.Ano
                    where c.Clvl > 90
                    select a).Distinct().Count();
        }

        public void Query3()
        {
            var ins = new Character()
            {
                Cname = "abc",
                Clvl = 90,
                Cmoney = 500
            };

            var table = db.GetTable<Character>();

            table.InsertOnSubmit(ins);
            db.SubmitChanges();

            var res =  from c in table
                       where c.Cno > 1300
                       select c;

            foreach (Character c in res)
                c.Print();
        }

        public void Query4()
        {
            var table = db.GetTable<Character>();

            var update = from c in table
                         where c.Cno > 1300
                         select c;

            foreach (Character c in update)
                c.Cname += "a";

            db.SubmitChanges();

            var res = from c in table
                      where c.Cno > 1300
                      select c;

            foreach (Character c in res)
                c.Print();
        }

        public void Query5()
        {
            var table = db.GetTable<Character>();

            var deletion = from c in table
                           where c.Cno > 1300
                           select c;

            table.DeleteAllOnSubmit(deletion);
            db.SubmitChanges();

            var res = from c in table
                      where c.Cno > 1300
                      select c;

            if (res.Count() == 0)
                Console.WriteLine("Everything was deleted!");
        }
        
        public double? Query6(int? num)
        {
            double? res = 0;
            db.GetDoubleFact(num, ref res);
            return res;
        }

        DataClasses1DataContext db = new DataClasses1DataContext();
    }
}
