﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Lab9
{
    class LinqToXml
    {
        public LinqToXml(string filename)
        {
            document = XDocument.Load(filename);
        }

        public string Query1()
        {
            return  (from e in document.Descendants("C")
                    where e.Element("Clvl").Value.Equals("99")
                    select e.Element("Cname")).First().Value;
        }

        public void Query2()
        {
            var elem = from e in document.Descendants("C")
                       select e.Element("Cname");
            foreach (var x in elem)
                x.Value = x.Value.Trim();
            document.Save("updxml.xml");
        }

        public void Query3()
        {
            var xe = new XElement("C");
            xe.Add(new XElement ("Cno", "1010"));
            xe.Add(new XElement ("Cname", "abc"));
            xe.Add(new XElement ("Clvl", "50"));
            xe.Add(new XElement ("Cmoney", "1000"));

            document.Element("ROOT").Add(xe);

            document.Save("insxml.xml");
        }

        private XDocument document;
    }
}
