﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Lab12
{
    [DataContract]
    public class PersonInfo
    {
        [DataMember]
        public string name;

        [DataMember]
        public string surname;

        [DataMember]
        public string number;
    }

    [DataContract]
    public class RootData
    {
        [DataMember]
        int count;

        [DataMember(Name = "persons")]
        PersonInfo[] AllPersons;
    }
}
