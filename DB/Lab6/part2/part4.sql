use [dbGameServer]
go

if exists (select * from sysobjects where id = object_id(N'dbo.DeleteViews'))
	drop procedure dbo.DeleteViews
go

create procedure dbo.DeleteViews
(
	@counter int output
)
as
begin
	declare @name sysname
	declare _cursor cursor
	global
	for
		select name
		from sys.objects O join syscomments C on O.object_id = C.id
		where type_desc like '%VIEW%' and encrypted = 0
	open _cursor

	fetch next from _cursor into @name
	while (@@FETCH_STATUS = 0)
	begin
		exec ('drop view dbo.' + @name)
		select @counter = @counter + 1
		fetch next from _cursor into @name
	end
	close _cursor
	deallocate _cursor
end
go

--Тестирование
if exists (select * from sysobjects where id = object_id(N'dbo.test1'))
	drop view dbo.test1

if exists (select * from sysobjects where id = object_id(N'dbo.test2'))
	drop view dbo.test2

if exists (select * from sysobjects where id = object_id(N'dbo.test3'))
	drop view dbo.test3
go

create view dbo.test1 as
select Cname, Aname
from
(C join CAS on C.Cno = CAS.Cno) join A on CAS.Ano = A.Ano
where
Clvl < 30
go

create view dbo.test2 as
select * from CAS
go

create view dbo.test3 with encryption as
select Sname
from S
where Smaxplayers < 10000
go

declare @counter int
set @counter = 0
exec dbo.DeleteViews @counter output
print @counter

if exists (select * from sysobjects where id = object_id(N'dbo.test1'))
	print 'test1 exists'

if exists (select * from sysobjects where id = object_id(N'dbo.test2'))
	print 'test2 exists'

if exists (select * from sysobjects where id = object_id(N'dbo.test3'))
	print 'test3 exists'
go

use [master]
go