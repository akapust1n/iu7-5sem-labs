use [dbGameServer]
go

if exists (select * from sysobjects where id = object_id(N'AfterTrigger'))
	drop trigger AfterTrigger

if exists (select * from sysobjects where id = object_id(N'InsteadTrigger'))
	drop trigger InsteadTrigger
go

create trigger AfterTrigger
on A
after insert
as
	update A set Aphash = cast(hashbytes('md5', cast(Aphash as nvarchar)) as int)
	where Ano in (select Ano from inserted)
go

create trigger InsteadTrigger
on C
instead of delete
as
	update C set Clvl = 1, Cmoney = 0 where Cno in (select Cno from deleted)
go

insert into A
(Aname, Aphash, AcrDate)
values
('abcdefghj', 123, '2000-01-01')
go

delete from C where Cno = 1

select * from A
where Ano > 1000

select * from C
where Cno = 1

delete from A
where Ano > 1000

use [master]
go