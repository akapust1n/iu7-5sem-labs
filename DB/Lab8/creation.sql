use [dbGameServer]
go

if exists  (select * from sys.objects 
			where name = 'MyXML'
			and type = 'U')
drop table [dbo].[MyXMl]


if exists (select * from sys.xml_schema_collections where name = 'SMyXML')
drop xml schema collection SMyXML
go

create xml schema collection SMyXML
as
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
        <xs:element name="A">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="Ano" type="xs:unsignedShort" />
              <xs:element name="Aname" type="xs:string" />
              <xs:element name="Aphash" type="xs:unsignedInt" />
              <xs:element name="AcrDate" type="xs:date" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
</xs:schema>'
go

create table [MyXML]
(
	[Mno][int] primary key identity(1, 1) not null,
	[XMLval][xml] (SMyXml) not null
)
go

create primary xml index XMLPrim
on [MyXML] (XMLval)

create xml index XMLPath
on [MyXML] (XMLval)
using xml index XMLPrim for path
go

use [master]
go