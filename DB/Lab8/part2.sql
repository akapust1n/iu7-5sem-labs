use [dbGameServer]
go

-----------------
declare @XML xml
set @xml =
'<ROOT></ROOT>'
select @xml.query('.')

set @xml.modify('insert <test>abc</test> into (/ROOT[1])')
select @xml.query('.')

set @xml.modify ('delete /ROOT/test')
select @xml.query('.')
go
-----------------------------------

update MyXML
set XMLval.modify ('replace value of (/A/Aname)[1] with ''test''')

select XMLval.value('/A[1]/Aname[1]', 'char(11)') from MyXML
go

use [master]
go