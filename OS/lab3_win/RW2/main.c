#include <stdio.h>
#include <windows.h>

#define READERS 4
#define WRITERS 4
#define ITER    3


HANDLE singleProcessMutex;
HANDLE evCanRead;
HANDLE evCanWrite;

HANDLE readerThreads[READERS];
HANDLE writerThreads[WRITERS];

int writersID[WRITERS];
int readersID[READERS];

unsigned int currReaders = 0, waitingReaders = 0;
int value = 0;

void CatchProcessMutex(void)
{
    WaitForSingleObject(singleProcessMutex, INFINITE);
}

void ReleaseProcessMutex(void)
{
    ReleaseMutex(singleProcessMutex);
}

void StartWrite(void)
{
    WaitForSingleObject(evCanWrite, INFINITE);
    CatchProcessMutex();
    ResetEvent(evCanWrite);
}

void StartRead(void)
{
    WaitForSingleObject(evCanRead, INFINITE);
    CatchProcessMutex();
}

void StopWrite(void)
{
    currReaders = READERS;
    SetEvent(evCanRead);

    ReleaseProcessMutex();
}

void StopRead(void)
{
        if (--currReaders == 0)
            SetEvent(evCanWrite);
    ReleaseProcessMutex();
}

DWORD WINAPI Reader(LPVOID param)
{
    int num = *(int *) param;

    for (int i = 0; i < WRITERS * ITER; ++i)
    {
        StartRead();
        printf ("Reader %d have read %d!\n", num, value);
        StopRead();
        Sleep(500);
    }
    return 0;
}

DWORD WINAPI Writer(LPVOID param)
{
    int num = *(int *) param;

    for (int i = 0; i < ITER; ++i)
    {
        StartWrite();
        printf ("Writer %d have written %d!\n", num, ++value);
        StopWrite();
    }
    return 0;
}

void InitHandles(void)
{
    singleProcessMutex = CreateMutex(NULL, FALSE, NULL);
    // ��������� ��� ������������ ��������� ����������, ��������� ���������, ��������

    evCanRead = CreateEvent(NULL, TRUE, TRUE, NULL);
    /* ��������� ��� ������������ ��������� ����������, ����� ������ ��������� �������,
       ��������� ���������, �������� */
    evCanWrite = CreateEvent(NULL, FALSE, TRUE, NULL);
}

void CreateThreads(void)
{
    for (int i = 0; i < WRITERS; ++i)
    {
        writersID[i] = i;
        writerThreads[i] = CreateThread(NULL, 0, Writer, (writersID + i), 0, NULL);
    }

    for (int i = 0; i < READERS; ++i)
    {
        readersID[i] = i;
        readerThreads[i] = CreateThread(NULL, 0, Reader, (readersID + i), 0, NULL);
    }
}

int main()
{
    InitHandles();
    CreateThreads();

    WaitForMultipleObjects(WRITERS, writerThreads, TRUE, INFINITE);
    WaitForMultipleObjects(READERS, readerThreads, TRUE, INFINITE);

    return 0;
}
