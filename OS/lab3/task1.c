#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	pid_t prc_pid;
	pid_t hie_pid;
	pid_t gr_id;
	char* form_string;
	
	pid_t fork_pid = fork();
	switch (fork_pid)
	{
		case -1:
			perror ("Fork failed!\n");
			exit(-1);
		case 0:
			prc_pid = getpid();
			hie_pid = getppid();
			gr_id = getpgrp();
			form_string = "PID %d, PPID %d, GRID %d\n";
		break;
		default:
			prc_pid = getpid();
			hie_pid = fork_pid;
			gr_id = getpgrp();
			form_string = "PID %d, Child ID %d, GRID %d\n";
	}
	
	for (int i = 0; i < 5; ++i)
	{
		printf (form_string, prc_pid, hie_pid, gr_id);
		sleep (1);
		if (fork_pid != 0)
		{
			printf("Parent process is finished!\n");
			break;
		}
		hie_pid = getppid();
	}
	
	return 0;
}
