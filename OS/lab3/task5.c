#define _POSIX_SOURCE
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int piped[2];
pid_t pid;

void child_sigusr1_catcher(int signum)
{
		pid_t pid = getpid();
		pid_t ppid = getppid();

		printf ("Child process with PID %d and PPID %d started processing!\n", pid, ppid);
				
		close (piped[1]);
		char c;
		while (read (piped[0], &c, 1), c != '\n')
			putc (c, stdout);
		putc ('\n', stdout);
		printf ("Child process with PID %d and PPID %d stopped processing and waiting for another signal!\n", pid, ppid);
		
		signal (SIGUSR1, child_sigusr1_catcher);
		kill (ppid, SIGUSR1);
}

void child_sigterm_catcher(int signum)
{
	printf ("Child process with PID %d and PPID %d finished!\n", getpid(), getppid());
	exit(0);
}

void parent_sigusr1_catcher(int signum)
{
	char msg[32];

	printf ("Type string to transmit or type \"Exit\" to exit\n");
	scanf ("%30s", msg);

	if (strcmp (msg, "Exit") == 0)
	{
		printf ("Parent process with PID %d finished!\n", getpid());
		kill (0, SIGTERM);
		exit(0);
	}
	
	int len = strlen(msg);
	msg[len++] = '\n';
	msg[len] = '\0';
	close (piped[0]);
	write (piped[1], msg, len);
	
	signal (SIGUSR1, parent_sigusr1_catcher);
	kill(pid, SIGUSR1);
}

void parent_report_death(void)
{
	printf ("Parent process with PID %d and child PID %d died!\n", getpid(), pid);
}

int main(void)
{	
	if (pipe (piped) == -1)
	{
		fprintf (stderr, "Pipe couldn't be created in process %d, exiting...\n", getpid());
		return -1;
	}
	pid = fork();
	
	switch (pid)
	{
		case -1:
			fprintf (stderr, "Fork failed in process %d, exiting...\n", getpid());
			return -2;
		case 0:
			signal (SIGTERM, child_sigterm_catcher);
			signal (SIGUSR1, child_sigusr1_catcher);
			
			while (1) {}
		default:
			signal (SIGUSR1, parent_sigusr1_catcher);			
			atexit (parent_report_death);

			raise (SIGUSR1);
			while (1) {}
	}
	return 0;
}
