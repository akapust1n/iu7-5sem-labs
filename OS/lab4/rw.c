#define _XOPEN_SOURCE

#include <stdio.h>

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <sys/shm.h>
 
void exit(int);
 
#define READERS 4
#define WRITERS 4
#define ITER    3

#define ACTIVE_READER  0
#define ACTIVE_WRITER  1
#define WRITER_QUEUE   2

#define SEMNUM         3

struct sembuf   start_reading[] = {{WRITER_QUEUE, 0, 0}, {ACTIVE_READER, 1, 0}},
				start_writing[] = {{WRITER_QUEUE, 1, 0}, {ACTIVE_READER, 0, 0}, {ACTIVE_WRITER, -1, 0}},
		
				stop_reading[]  = {{ACTIVE_READER, -1, 0}},
				stop_writing[]  = {{ACTIVE_WRITER, 1, 0}, {WRITER_QUEUE, -1, 0}};
				
int shm_id = -1, sem = -1;
int* shar_mem = NULL;

unsigned short	start_value[] = {0, 1, 0};

void debug_puts(const char* s)
{
	#ifdef DEBUG
		puts (s);
	#endif
}

void terminate(int signal)
{
	while (wait(NULL) != -1) {}
	
	printf ("All child processes of process %d finished!\n", getpid());
	
	if (sem != -1)
		semctl(sem, 0, IPC_RMID); 		// deleting semaphore command
	else
		exit(signal);
		
	if (shm_id != -1)
		shmdt(shar_mem); 				// disabling shared memory
	else
		exit(signal);
	
	if (shar_mem != (int *) -1)
		shmctl(shm_id, 0, IPC_RMID); 	// deleting shared memory
	else
		exit(signal);
	
	exit(0);
}

void start_read(int sem)
{
	debug_puts ("Entering start_read...");
	semop(sem, start_reading, 2);
}

void start_write(int sem)
{
	debug_puts ("Entering start_write...");
	semop(sem, start_writing, 3);
}

void stop_read(int sem)
{
	debug_puts("Entering stop_read...");
	semop(sem, stop_reading, 1);
}

void stop_write(int sem)
{
	debug_puts ("Entering stop_write...");
	semop (sem, stop_writing, 2);
}

void reader(int sem, int* shar_mem, int num)
{
		
	for (int i = 0; i < ITER; ++i)
	{
		start_read(sem);
		printf ("Reader %d has read %d from shared memory!\n", num, *shar_mem);
		stop_read(sem);
		
		sleep (1);
	}
	
	exit(0);
}

void writer (int sem, int* shar_mem, int num)
{
	for (int i = 0; i < ITER; ++i)
	{
		start_write(sem);
		printf("Writer %d has changed value in shared memory to %d\n", num, ++(*shar_mem));
		stop_write(sem);
		sleep (2);
	}
	
	exit(0);
}

int main(void)
{
	int perms = IPC_CREAT | S_IRWXU | S_IRWXG | S_IRWXO;
	
	if ((sem = semget(IPC_PRIVATE, SEMNUM, perms)) == -1)
    {
        perror("Semget error \n"); 
        terminate(1);
    }

    semctl(sem, 0, SETALL, start_value);
	
	if ((shm_id = shmget(IPC_PRIVATE, sizeof(int), perms)) == -1)
	{
		perror ("Shmget error!\n");
		terminate(2);
	}
	
	if ((shar_mem = (int *) shmat(shm_id, 0, 0)) == NULL)
	{
		perror ("Shmat error!\n");
		terminate(3);
	}
	
	*shar_mem = 0;
	
	signal(SIGABRT, &terminate);
    signal(SIGTERM, &terminate);
    signal(SIGINT, &terminate); 
    signal(SIGKILL, &terminate);

	pid_t pid;
	
	for (int i = 0 ; i < WRITERS; ++i)
		if ((pid = fork()) != -1)
			if (!pid)
				writer(sem, shar_mem, i);
			else
				;
		else
			perror ("Fork error while creating writers!\n");
			
		
	for (int i = 0; i < READERS; ++i)
		if ((pid = fork()) != -1)
			if (!pid)
				reader(sem, shar_mem, i);
			else
				;
		else
			perror ("Fork error while creating readers!\n");
	
	terminate(0);
}
