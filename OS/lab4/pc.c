#define _XOPEN_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <sys/shm.h>
 
#define BUF_SIZE 5
  
#define RUN_ACCESS 0
#define BUF_LEFT 1
#define BUF_CURR 2
 
unsigned short start_value[] = {1, BUF_SIZE, 0};
 
 
  // структура, задающая операцию на семафоре
struct sembuf PProducer[2] = {{BUF_LEFT,  -1, 0}, {RUN_ACCESS, -1, 0}};
struct sembuf VProducer[2] = {{BUF_CURR,  1, 0},  {RUN_ACCESS,  1, 0}};
struct sembuf PConsumer[2] = {{BUF_CURR, -1, 0},  {RUN_ACCESS, -1, 0}};
struct sembuf VConsumer[2] = {{BUF_LEFT,  1, 0},  {RUN_ACCESS,  1, 0}};

int main()
{
    int flags = S_IRWXU | S_IRWXG | S_IRWXO;
 
    int sem = semget(IPC_PRIVATE, 3, IPC_CREAT | flags);
    if (sem == -1) 
	{
		perror("Semget error!\n");
		return 1;
	}
 
    if (semctl(sem, 0, SETALL, start_value) == -1)
	{
		perror("Semctl error!\n");
		return 2;
	}
 
    int shm_id = shmget(IPC_PRIVATE, (BUF_SIZE) * sizeof(int), IPC_CREAT | flags);
    if (shm_id == -1)
	{
		perror("Shmget error!\n");
		return 3;
	}
     
    int* buf = (int *) shmat(shm_id, 0, 0);
    if (buf == (int *)(-1))
	{
		perror("Shmat error!\n");
		return 1; 
	}
     
    pid_t child = 0;
    if ((child = fork()) == -1) 
	{
		perror("Fork error!\n");
		return 1;
	}
     
    if (child == 0)
    {
		#define MAX_VAL 10
        int data = 1;
        while(1)
        {
            semop(sem, PProducer, 2);
			
			int pos = semctl(sem, BUF_CURR, GETVAL);
			buf[pos] = data;
            printf("Producer put %d, position %d\n", data, pos);
			
			semop(sem, VProducer, 2);
			
			data = (data + 1) % (MAX_VAL + 1);
        }
        #undef MAX_VAL
    }
    else
    {
        while(1)
        {
            semop(sem, PConsumer, 2);
             
            int pos = semctl(sem, BUF_CURR, GETVAL);
            printf("Consumer get %d, position %d\n", buf[pos], pos);
           
            semop(sem, VConsumer, 2);
        }   
    }
    return 0;
}
