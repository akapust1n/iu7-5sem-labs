#define _XOPEN_SOURCE

#include <stdio.h>

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <sys/shm.h>
 
void exit(int);
 
#define READERS 4
#define WRITERS 4
#define ITER    3

#define REQUIRED_READER 0

struct sembuf   start_reading[] = {{REQUIRED_READER, -2, 0}},
				start_writing[] = {{REQUIRED_READER, 0, 0}, {REQUIRED_READER, 1, 0}},
		
				stop_reading[]  = {{REQUIRED_READER, -1, 0}},
				stop_writing[]  = {{REQUIRED_READER, 3 * READERS - 1, 0}};
				
int shm_id = -1, sem = -1;
int* shar_mem = NULL;

void debug_puts(const char* s)
{
	#ifdef DEBUG
		puts (s);
	#endif
}

void terminate(int signal)
{
	while (wait(NULL) != -1) {}
	
	printf ("All child process of processes %d finished!\n", getpid());
	
	if (sem != -1)
		semctl(sem, 0, IPC_RMID); 		// deleting semaphore command
	else
		exit(signal);
		
	if (shm_id != -1)
		shmdt(shar_mem); 				// disabling shared memory
	else
		exit(signal);
	
	if (shar_mem != (int *) -1)
		shmctl(shm_id, 0, IPC_RMID); 	// deleting shared memory
	else
		exit(signal);
	
	exit(0);
}

void start_read(int sem)
{
	debug_puts ("Entering start_read...\n");
	semop(sem, start_reading, 1);
}

void start_write(int sem)
{
	debug_puts ("Entering start_write...\n");
	semop(sem, start_writing, 2);
}

void stop_read(int sem)
{
	debug_puts("Entering stop_read...\n");
	semop(sem, stop_reading, 1);
}

void stop_write(int sem)
{
	debug_puts ("Entering stop_write...\n");
	semop (sem, stop_writing, 1);
}

void reader(int sem, int* shar_mem, int num)
{
		
	for (int i = 0; i < WRITERS * ITER; ++i)
	{
		start_read(sem);
		printf ("Reader %d has read %d from shared memory!\n", num, *shar_mem);
		stop_read(sem);
		sleep (1);
	}
	
	exit(0);
}

void writer (int sem, int* shar_mem, int num)
{
	for (int i = 0; i < ITER; ++i)
	{
		start_write(sem);
		printf("Writer %d has changed value in shared memory to %d\n", num, ++(*shar_mem));
		stop_write(sem);
	}
	
	exit(0);
}

int main(void)
{
	int perms = IPC_CREAT | S_IRWXU | S_IRWXG | S_IRWXO;
	
	if ((sem = semget(IPC_PRIVATE, 1, perms)) == -1)
    {
        perror("Semget error \n"); 
        terminate(1);
    }

    semctl(sem, REQUIRED_READER, SETVAL, 0);
	
	if ((shm_id = shmget(IPC_PRIVATE, sizeof(int), perms)) == -1)
	{
		perror ("Shmget error!\n");
		terminate(2);
	}
	
	if ((shar_mem = (int *) shmat(shm_id, 0, 0)) == NULL)
	{
		perror ("Shmat error!\n");
		terminate(3);
	}
	
	*shar_mem = 0;
	
	signal(SIGABRT, &terminate);
    signal(SIGTERM, &terminate);
    signal(SIGINT, &terminate); 
    signal(SIGKILL, &terminate);

	pid_t pid;
	
	for (int i = 0 ; i < WRITERS; ++i)
		if ((pid = fork()) != -1)
			if (!pid)
				writer(sem, shar_mem, i);
			else
				;
		else
			perror ("Fork error while creating writers!\n");
			
		
	for (int i = 0; i < READERS; ++i)
		if ((pid = fork()) != -1)
			if (!pid)
				reader(sem, shar_mem, i);
			else
				;
		else
			perror ("Fork error while creating readers!\n");
	
	terminate(0);
}
