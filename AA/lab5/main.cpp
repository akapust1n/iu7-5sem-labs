#include <iostream>
#include <cstdlib>

using namespace std;

static inline int min(int a, int b, int c)
{
    if ((a <= b) && (a <= c))
        return a;
    else if (b < c)
        return b;
    else
        return c;
}

static inline int m(char a, char b) { return (a == b) ? 0 : 1; }

static int Levenstein(const string& str1, const string& str2)
{
    int len1 = str1.length();
    int len2 = str2.length();

    int size = len1 + 1;

    int prev[size];
    int curr[size];

    for (int i = 0; i < size; ++i)
        prev[i] = i;

    size_t copySize = size * sizeof(int);

    for (int i = 0; i < len2; ++i)
    {
        curr[0] = i + 1;

        for (int j = 1; j < size; ++j)
            curr[j] = min(curr[j - 1] + 1, prev[j] + 1, prev[j - 1] + m(str1[j - 1], str2[i]));
        memcpy(prev, curr, copySize);
    }

    return prev[len1];
}

static int DamerauLevenstein(const string& str1, const string& str2)
{
    int len1 = str1.length();
    int len2 = str2.length();

    if (!len1)
        return len2;
    if (!len2)
        return len1;

    int size = len1 + 1;

    int prev2[size];
    int prev[size];
    int curr[size];

    for (int i = 0; i < size; ++i)
        prev2[i] = i;

    prev[0] = 1;

    for (int i = 1; i < size; ++i)
        prev[i] = min (prev[i - 1] + 1, prev2[i] + 1, prev2[i - 1] + m(str1[i - 1], str2[0]));

    size_t copySize = size * sizeof(int);

    for (int i = 1; i < len2; ++i)
    {
        curr[0] = i + 1;
        curr[1] = min (curr[0] + 1, prev[1] + 1, prev[0] + m (str1[0], str2[i]));

        for (int j = 2; j < size; ++j)
        {
            curr[j] = min (curr[j - 1] + 1, prev[j] + 1, prev[j - 1] + m(str1[j - 1], str2[i]));
            if ((str1[j - 2] == str2[i]) && (str1[j - 1] == str2[i - 1]))
                curr[j] = min (curr[j], prev2[j - 2] + 1);
        }

        memcpy(prev2, prev, copySize);
        memcpy(prev, curr, copySize);
    }

    return prev[len1];
}

int main()
{
    string str1, str2;

    cout << "Input two strings" << endl;

    getline(cin, str1);
    getline(cin, str2);

    int res = Levenstein(str1, str2);

    cout  << "Levenstein distance: " << res << endl;

    res = DamerauLevenstein(str1, str2);

    cout << "Damerau-Levenstein distance: " << res << endl;

    return 0;
}
