#ifndef __MAIN_H__
#define __MAIN_H__

#include <windows.h>

/*  To use this exported function of dll, include this header
 *  in your project.
 */

#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT __declspec(dllimport)
#endif


#ifdef __cplusplus
extern "C"
{
#endif

class DLL_EXPORT TickTimer
{
    public:
        TickTimer();
        virtual ~TickTimer();

        virtual void Start();
        virtual void Stop();
        virtual void Restart();
        virtual unsigned long long GetTicks() const;
    private:
        virtual unsigned long long Tick() const;

        unsigned long long startTick;
        unsigned long long stopTick;
        bool isStarted;
};


#ifdef __cplusplus
}
#endif

#endif // __MAIN_H__
