#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>

#include "searchfuncs.h"

#include "stringgener.h"

static inline char GetRandomChar(char min, char max) { return (rand() % (max - min + 1)) + min;}

static void CreateString (int len, char* buf)
{
    for (int i = 0; i < len; ++i, ++buf)
        *buf = GetRandomChar('a', 'z');
    *buf = '\0';
}

static inline void DeleteAllSubstrings(char* haystack, char* needle)
{
    int pos = ClassicSearch(haystack, needle).pos;
    while (pos != -1)
    {
        ++haystack[pos];
        pos = ClassicSearch(haystack, needle).pos;
    }
}

static inline void GuaranteeOneSubstring(char* haystack, char* needle, int pos, int nLen)
{
    DeleteAllSubstrings(haystack, needle);

    memcpy(haystack + pos, needle, nLen);
}

void GenerateStrings(int hLen, int nLen, HaystackType hType, int symbNum, SearchStrings* buf)
{
    static char wasInit = 0;
    if (!wasInit)
    {
        srand(time(NULL));
        ++wasInit;
    }


    if (nLen >= hLen)
    {
        buf->haystack = NULL;
        buf->needle = NULL;
        return;
    }

    CreateString(hLen, buf->haystack);
    CreateString(nLen, buf->needle);

    switch (hType)
    {
    case hsNoNeedle:
        {
            DeleteAllSubstrings(buf->haystack, buf->needle);
            break;
        }
    case hsRandom:
        {
            int pos = ClassicSearch(buf->haystack, buf->needle).pos;
            if (pos == -1)
                GuaranteeOneSubstring(buf->haystack, buf->needle, rand() % (hLen - nLen + 1), nLen);
            break;
        }
    case hsAtStart:
        {
            memcpy(buf->haystack, buf->needle, nLen);
            break;
        }
    case hsAtEnd:
        {
            DeleteAllSubstrings(buf->haystack, buf->needle);

            int dist = hLen - nLen;
            memcpy(buf->haystack + dist, buf->needle, nLen);
            break;
        }
    case hsSymbFail:
        {
            int pos = rand() % (hLen - nLen + 1);
            GuaranteeOneSubstring(buf->haystack, buf->needle, pos, nLen);

            ++(buf->haystack[pos + symbNum]);
            break;
        }
    case hsSpecialCase1:
        {
            memcpy(buf->haystack, "trabant", 8);
            memcpy(buf->needle, "ababc", 6);

            break;
        }

    case hsSpecialCase2:
        {
            memcpy(buf->haystack, "abababc", 8);
            memcpy(buf->haystack, "ababc", 6);

            break;
        }
    default:
        ;
    }

}
