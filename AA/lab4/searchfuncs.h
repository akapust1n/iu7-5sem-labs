#ifndef SEARCHFUNCS_H_INCLUDED
#define SEARCHFUNCS_H_INCLUDED

typedef struct
{
    int pos;
    int compCount;
} SearchResult;

typedef SearchResult (*SearchFunction)(const char*, const char*);

SearchResult ClassicSearch(const char* haystack, const char* needle);
SearchResult KMPSearch(const char* haystack, const char* needle);
SearchResult BMSearch(const char* haystack, const char* needle);

#endif // SEARCHFUNCS_H_INCLUDED
