#ifndef MATRIX_H
#define MATRIX_H

#include "SettableWxGrid.h"

class Matrix
{
    public:
        Matrix (int i, int j);
        virtual ~Matrix();
        Matrix(const Matrix& other);
        Matrix& operator=(const Matrix& other);

        void Fill (double min, double max);
        void Print (SettableWxGrid& grid) const;

        double& operator() (int i, int j) const;
        int GetM() const { return m; }
        int GetN() const { return n; }
    protected:          // protected ������ ��������� - ������� � ���������� �� ���� ����� ������������ ��������� - ��� ����� ��������������
        double** values;
        int m, n;

        void AllocMatr (int m, int n);
        void FreeMatr();
};

#endif // MATRIX_H
