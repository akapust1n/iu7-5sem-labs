#ifndef __SETTABLEWXGRID_H__
#define __SETTABLEWXGRID_H__

#include <wx/grid.h>

class SettableWxGrid: public wxGrid
{
    public:
        SettableWxGrid(wxPanel*& p, const long int& i, const wxPoint& pt, const wxSize& s, int j, const wchar_t w[9]):
            wxGrid (p, i, pt, s, j, w) {}

        void SetCols (int num)
        {
            int curr = GetNumberCols();
            int diff = abs (num - curr);

            if ((num < 0) || (diff == 0))
                return;

            if (num < curr)
                DeleteCols (0, diff, true);
            else
                AppendCols (diff, true);
        }

        void SetRows (int num)
        {
            int curr = GetNumberRows();
            int diff = abs (num - curr);

            if ((num < 0) || (diff == 0))
                return;

            if (num < curr)
                DeleteRows (0, diff, true);
            else
                AppendRows (diff, true);
        }
};
#endif // __SETTABLEWXGRID_H__
