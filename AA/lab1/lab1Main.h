/***************************************************************
 * Name:      lab1Main.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2014-09-17
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef LAB1MAIN_H
#define LAB1MAIN_H

#include "SettableWxGrid.h"
#include "multmax.h"

//(*Headers(lab1Frame)
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/grid.h>
#include <wx/choice.h>
#include <wx/button.h>
#include <wx/frame.h>
//*)

class lab1Frame: public wxFrame
{
    public:

        lab1Frame(wxWindow* parent,wxWindowID id = -1);
        virtual ~lab1Frame();

    private:

        //(*Handlers(lab1Frame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnbtnMakeMatrix1Click(wxCommandEvent& event);
        void OnbtnMakeClick(wxCommandEvent& event);
        void OnbtnMultClick(wxCommandEvent& event);
        void OnbtnOutClick(wxCommandEvent& event);
        //*)

        //(*Identifiers(lab1Frame)
        static const long ID_STATICTEXT1;
        static const long ID_TEXTCTRL1;
        static const long ID_STATICTEXT2;
        static const long ID_TEXTCTRL2;
        static const long ID_BUTTON1;
        static const long ID_BUTTON2;
        static const long ID_GRID1;
        static const long ID_CHOICE1;
        static const long ID_BUTTON3;
        static const long ID_STATICTEXT3;
        static const long ID_STATICTEXT4;
        static const long ID_CHOICE2;
        static const long ID_BUTTON4;
        static const long ID_PANEL1;
        //*)

        //(*Declarations(lab1Frame)
        wxButton* btnMakeMatrix1;
        wxStaticText* StaticText2;
        wxButton* btnMake;
        wxPanel* Panel1;
        wxTextCtrl* tcCols;
        wxStaticText* StaticText1;
        wxTextCtrl* tcRows;
        wxStaticText* StaticText3;
        wxButton* btnOut;
        wxChoice* chOut;
        wxChoice* chMult;
        wxButton* btnMult;
        SettableWxGrid* swgMatrix;
        wxStaticText* stTime;
        //*)

        MultMatrix* matr1 = 0;
        MultMatrix* matr2 = 0;
        MultMatrix* mult = 0;

        DECLARE_EVENT_TABLE()
};

#endif // LAB1MAIN_H
