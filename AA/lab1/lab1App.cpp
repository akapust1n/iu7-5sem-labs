/***************************************************************
 * Name:      lab1App.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2014-09-17
 * Copyright:  ()
 * License:
 **************************************************************/

#include "lab1App.h"

//(*AppHeaders
#include "lab1Main.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(lab1App);

bool lab1App::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	lab1Frame* Frame = new lab1Frame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
