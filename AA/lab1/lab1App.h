/***************************************************************
 * Name:      lab1App.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2014-09-17
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef LAB1APP_H
#define LAB1APP_H

#include <wx/app.h>

class lab1App : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // LAB1APP_H
