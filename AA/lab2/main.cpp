#include <iostream>
#include <cstdlib>
#include "time.h"
#include "../ticktimer.h"

inline void swap (int* array, int i, int j)
{
    int buf = array[i];
    array[i] = array[j];
    array[j] = buf;
}

inline void BubbleSort (int* array, int n)
{
    for (int i = 0; i < n - 1; ++i)
        for (int j = 1; j < n - i; ++j)
            if (array[j - 1] > array[j])
                swap (array, j - 1, j);
}

void SiftDown (int* array, int i, int j);

inline void HeapSort (int* array, int n)
{
    for (int i = n / 2 - 1; i >= 0; --i)
        SiftDown(array, i, n);

    for (int i = n - 1; i >= 1; --i)
    {
        swap (array, 0, i);
        SiftDown(array, 0, i);
    }
}

inline void SiftDown (int* array, int i, int j)
{
    bool done = false;
    int maxChild;

    while ((i * 2 + 1 < j) && (!done))
    {
        if (i * 2 + 1 == j - 1)
            maxChild = i * 2 + 1;
        else if (array[i * 2 + 1] > array[i * 2 + 2])
            maxChild = i * 2 + 1;
        else
            maxChild = i * 2 + 2;

        if (array[i] < array[maxChild])
        {
            swap (array, i, maxChild);
            i = maxChild;
        }
        else
            done = true;
    }
}

int* GenerateBest(int n)
{
    int* res = new int[n];

    for (int i = 0; i < n; ++i)
        res[i] = i;
    return res;
}

int* GenerateWorst(int n)
{
    int* res = new int[n];

    for (int i = 0; i < n; ++i)
        res[i] = n - i;
    return res;
}

int* GenerateRand(int n)
{
    int* res = new int[n];

    for (int i = 0; i < n; ++i)
        res[i] = rand();
    return res;
}

inline double MultipleTest (int*(*Gener)(int), void (*Sort)(int*, int), int n, int size)
{
    double res = 0;

    TickTimer timer;

    for (int i = 0; i < n; ++i)
    {
        int* arr = Gener(size);
        timer.Restart();
        Sort(arr, size);
        timer.Stop();
        res += timer.GetTicks();
        delete[] arr;
    }

    return res / n;
}

int main(void)
{
    using namespace std;

    const int checks = 500;
    srand(time(0));

    for (int i = 100; i <= 1000; i += 100)
    {
        cout << "Size: " << i << endl;
        cout << "Bubblesort: " << MultipleTest(GenerateBest, BubbleSort, checks, i) << ' ';
        cout << MultipleTest(GenerateRand, BubbleSort, checks, i) << ' ';
        cout << MultipleTest (GenerateWorst, BubbleSort, checks, i) << endl;

        cout << "Heapsort: " << MultipleTest(GenerateBest, HeapSort, checks, i) << ' ';
        cout << MultipleTest(GenerateRand, HeapSort, checks, i) << ' ';
        cout << MultipleTest(GenerateWorst, HeapSort, checks, i) << endl;
    }
    return 0;
}
